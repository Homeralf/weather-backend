import asyncio

from tests.base import BaseTestCase
from tests.generators.city import generate_cities

from backend.models.city import insert_cities, get_cities


class CitiesTestCase(BaseTestCase):

    def test_get_cities(self):
        cities = generate_cities(n_cities=10)

        async def test():
            await insert_cities(db=self.db, cities=cities)
            result_cities = await get_cities(db=self.db)
            self.assertEqual(cities, result_cities)

        asyncio.get_event_loop().run_until_complete(test())
