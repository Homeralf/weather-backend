import asyncio

from tests.base import BaseTestCase
from tests.generators.city import generate_cities
from tests.generators.weather import generate_weathers

from backend.models.city import insert_cities
from backend.models.weather import insert_weathers, get_weathers_by_city_name


class WeatherTestCase(BaseTestCase):

    def test_get_weathers_by_city_name(self):
        cities = generate_cities(n_cities=10)
        weathers = generate_weathers(cities=cities, n_weathers_city=20)

        async def test():
            await insert_cities(db=self.db, cities=cities)
            await insert_weathers(db=self.db, weathers=weathers)
            for city in cities:
                expected_weathers = [weather for weather in weathers if weather.city == city]
                result_weathers = await get_weathers_by_city_name(db=self.db, city_name=city.name)
                self.assertEqual(expected_weathers, result_weathers)

        asyncio.get_event_loop().run_until_complete(test())
