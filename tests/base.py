import asyncio
import unittest

import asyncpg


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        async def connect():
            return await asyncpg.connect(host='127.0.0.1',
                                         port='5432',
                                         database='weather',
                                         user='test',
                                         password='test')

        self._db = asyncio.get_event_loop().run_until_complete(connect())

    def tearDown(self):
        async def disconnect():
            await self.db.execute("""DELETE FROM weather""")
            await self.db.execute("""DELETE FROM city""")
        asyncio.get_event_loop().run_until_complete(disconnect())

    @property
    def db(self) -> asyncpg.Connection:
        return self._db
