from typing import List
from uuid import uuid4

from backend.models.city import City


def generate_cities(n_cities: int) -> List[City]:
    return [City(id_=str(uuid4()), name=str(uuid4())) for _ in range(n_cities)]
