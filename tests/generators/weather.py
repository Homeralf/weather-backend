from typing import List
from uuid import uuid4
import random

from backend.models.city import City
from backend.models.weather import Weather


def generate_weathers(cities: List[City], n_weathers_city: int) -> List[Weather]:
    weathers = []
    for city in cities:
        weathers += [Weather(id_=str(uuid4()), temperature=round(float((random.random() * 100) - 50), 2), city=city)
                     for _ in range(n_weathers_city)]
    return weathers
