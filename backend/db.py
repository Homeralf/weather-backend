import asyncpg
from quart import current_app, g
import backend.config as config

DATABASE_KEY = "db"


async def connect_db():
    if DATABASE_KEY not in g:
        g.db = await get_postgres_connection()
    return g.db


def close_db(e=None):
    db = g.pop(DATABASE_KEY, None)
    if db is not None:
        db.close()


async def get_postgres_connection():
    return await asyncpg.connect(host=current_app.config[config.EnvironmentVariable.POSTGRES_HOST.value],
                                 port=current_app.config[config.EnvironmentVariable.POSTGRES_PORT.value],
                                 database=current_app.config[config.EnvironmentVariable.POSTGRES_DB.value],
                                 user=current_app.config[config.EnvironmentVariable.POSTGRES_USER.value],
                                 password=current_app.config[config.EnvironmentVariable.POSTGRES_PASSWORD.value])


def _disconnect_postgres():
    pass
