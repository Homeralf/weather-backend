CREATE TABLE city (
    id UUID NOT NULL PRIMARY KEY,
    name_ TEXT NOT NULL UNIQUE
);

CREATE TABLE weather (
    id UUID NOT NULL PRIMARY KEY,
    temperature FLOAT NOT NULL,
    city UUID REFERENCES city
);
