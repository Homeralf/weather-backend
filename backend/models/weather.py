from typing import List, Dict
from enum import Enum

import asyncpg

from backend.models.city import get_city_by_name, City


class WeatherColumnDB(Enum):
    ID = 'id'
    TEMPERATURE = 'temperature'
    CITY = 'city'


class Weather:
    def __init__(self, id_: str, temperature: float, city: City):
        self._id = id_
        self._temperature = temperature
        self._city = city

    @property
    def id(self):
        return self._id

    @property
    def temperature(self) -> float:
        return self._temperature

    @property
    def city(self) -> City:
        return self._city

    def to_json(self) -> Dict:
        return {"id": self.id, "temperature": self.temperature, "city": self.city.to_json()}

    def __eq__(self, other: 'Weather'):
        return self.id == other.id and self.temperature == other.temperature and self.city == other.city

    def __repr__(self):
        return f"(Weather id: {self.id}, temperature: {self.temperature}, city: {self.city})"


# region db

async def get_weathers_by_city_name(db: asyncpg.Connection, city_name: str) -> List[Weather]:
    city = await get_city_by_name(db=db, city_name=city_name)
    rows = await db.fetch(f"""
                          SELECT id, temperature, city
                          FROM weather
                          WHERE city = $1
    """, city.id)
    weathers = [Weather(id_=str(row[WeatherColumnDB.ID.value]),
                    temperature=float(row[WeatherColumnDB.TEMPERATURE.value]),
                    city=city) for row in rows]
    return weathers


async def insert_weathers(db: asyncpg.Connection, weathers: List[Weather]):
    await db.executemany(f"""
                            INSERT INTO weather ({WeatherColumnDB.ID.value}, {WeatherColumnDB.TEMPERATURE.value}, 
                            {WeatherColumnDB.CITY.value}) 
                            VALUES ($1, $2, $3)
                         """,
                         [(weather.id, weather.temperature, weather.city.id) for weather in weathers])
# endregion
