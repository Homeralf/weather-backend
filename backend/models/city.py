from typing import List, Dict
from enum import Enum

import asyncpg


class CityColumnDB(Enum):
    ID = 'id'
    NAME = 'name_'


class City:
    def __init__(self, id_: str, name: str):
        self._id = id_
        self._name = name

    @property
    def id(self):
        return self._id

    @property
    def name(self) -> str:
        return self._name

    def to_json(self) -> Dict:
        return {"id": self.id, "name": self.name}

    def __eq__(self, other: 'City'):
        return self.id == other.id and self.name == other.name

    def __repr__(self):
        return f"(City id: {self.id}, name: {self.name})"

# region db


async def get_cities(db: asyncpg.Connection) -> List[City]:
    cities = [City(id_=str(record[CityColumnDB.ID.value]),
                 name=record[CityColumnDB.NAME.value])
            for record in await db.fetch("""
                                            SELECT id, name_ 
                                            FROM city""")]
    return cities


async def get_city_by_id(db: asyncpg.Connection, city_id: str) -> City:
    row = await db.fetchrow("""
                            SELECT id, name_ FROM city WHERE id = $1""", city_id)
    return City(id_=str(row[CityColumnDB.ID.value]), name=row[CityColumnDB.NAME.value])


async def get_city_by_name(db: asyncpg.Connection, city_name: str) -> City:
    row = await db.fetchrow("""
                            SELECT id, name_ FROM city WHERE name_ = $1""", city_name)
    return City(id_=str(row[CityColumnDB.ID.value]), name=row[CityColumnDB.NAME.value])


async def insert_cities(db: asyncpg.Connection, cities: List[City]):
    await db.executemany(f"""
                            INSERT INTO city ({CityColumnDB.ID.value}, {CityColumnDB.NAME.value}) 
                            VALUES ($1, $2)
                         """,
                         [(city.id, city.name) for city in cities])
# endregion