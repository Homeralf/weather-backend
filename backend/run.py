from quart import Quart

import backend.config as config
import backend.db as db
import backend.api as api
from backend.loader.load import load_database


app = Quart(__name__)


@app.before_serving
async def startup():
    app.config.update(config.get_config_from_environment())
    async with app.app_context():
        await db.connect_db()
        await load_database()
        api.register_api(app)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')



