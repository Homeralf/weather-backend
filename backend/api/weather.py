import json

from quart import Quart, g

from backend.models.weather import get_weathers_by_city_name


def register(app: Quart):
    db = g.db

    @app.route('/weather/<city_name>')
    async def get_weather_from_city_endpoint(city_name: str):
        return json.dumps([weather.to_json()
                           for weather in (await get_weathers_by_city_name(db=db, city_name=city_name))])
