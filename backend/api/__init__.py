from backend.api import weather as weather_api
from backend.api import cities as cities_api


def register_api(app):
    weather_api.register(app)
    cities_api.register(app)
