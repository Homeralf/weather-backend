import json

from quart import Quart, g

from backend.models.city import get_cities


def register(app: Quart):
    db = g.db

    @app.route('/cities')
    async def get_cities_endpoint():
        return json.dumps([city.to_json() for city in (await get_cities(db=db))])
