from enum import Enum
import os
from typing import Dict


class EnvironmentVariable(Enum):
    POSTGRES_HOST = "POSTGRES_HOST"
    POSTGRES_PORT = "POSTGRES_PORT"
    POSTGRES_DB = "POSTGRES_DB"
    POSTGRES_USER = "POSTGRES_USER"
    POSTGRES_PASSWORD = "POSTGRES_PASSWORD"


def get_config_from_environment() -> Dict:
    config = {}
    for environment_variable in EnvironmentVariable:
        if os.environ.get(environment_variable.value) is None:
            raise EnvironmentError(f"Required environment variable {environment_variable.value} is not defined!")
        config[environment_variable.value] = os.environ.get(environment_variable.value)
    return config
