from enum import Enum
import json
import logging
import os

from quart import g
import requests
from uuid import uuid4


from backend.models.city import City, insert_cities, get_cities
from backend.models.weather import Weather, insert_weathers


LANGUAGE = "en"
API_KEY = "a5G4444qXz4sWe7"
JSON_FILE = "barcelona-localities.json"
JSON_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), JSON_FILE)


class CityAttribute(Enum):
    NAME = "Name"
    ID = "ID"


async def load_database():
    """
    Loads the database from api.tutiempo.net. Not production code.  # TODO: Refactor so it's not a mess
    """
    with open(JSON_PATH, 'r') as json_file:
        current_cities_names = [city.name for city in await get_cities(db=g.db)]
        cities_dict = json.load(json_file)
        new_cities_dict = [city_dict for city_dict in cities_dict
                           if city_dict[CityAttribute.NAME.value] not in current_cities_names]

        if new_cities_dict:
            new_cities = [City(id_=str(uuid4()), name=city_dict[CityAttribute.NAME.value])
                          for city_dict in cities_dict
                          if city_dict[CityAttribute.NAME.value] not in current_cities_names]
            await insert_cities(db=g.db, cities=new_cities)
            for new_city_dict in new_cities_dict:
                response = requests.request("GET",
                                            f"https://api.tutiempo.net/json/?lan={LANGUAGE}&apid={API_KEY}&lid={new_city_dict[CityAttribute.ID.value]}")
                response_json = response.json()
                temperature = float(
                    (response_json['day1']['temperature_max'] + response_json['day1']['temperature_min']) / 2)
                weather = Weather(id_=str(uuid4()), temperature=temperature,
                                  city=next(c for c in new_cities
                                            if c.name == new_city_dict[CityAttribute.NAME.value]))
                await insert_weathers(db=g.db, weathers=[weather])
        else:
            logging.info("Cities already loaded")
